package com.concordia.crud.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.concordia.crud.dto.OrderItemDTO;
import com.concordia.exception.ServiceException;

/**
 * Order Item Service Interface
 *
 * @author Alexis
 *
 */

public interface OrderItemService {

	OrderItemDTO getOrderItem(Long id) throws ServiceException;

	void saveOrderItem(OrderItemDTO orderItemDTO) throws ServiceException;

	void deleteOrderItem(Long id) throws ServiceException;

	void updateOrderItem(OrderItemDTO orderItemDTO) throws ServiceException;

	List<OrderItemDTO> getAllOrderItems() throws ServiceException;

	Page<OrderItemDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection)
			throws ServiceException;

}
